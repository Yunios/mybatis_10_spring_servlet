package com.tz.mybatis.framework.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Application Lifecycle Listener implementation class SpringContextListener
 *
 */
@WebListener
public class SpringContextListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public SpringContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent servletContextEvent)  {
    	/**
    	 * 步骤 1:创建ioc容器
    	 * 2:将容器对象放入ServletContext(appliaction)
    	 */
    	ApplicationContext ac =  new ClassPathXmlApplicationContext("applicationContext.xml");
    	
    	//将容器对象翻入ServletContext中
    	ServletContext servletContext = servletContextEvent.getServletContext();
    	servletContext.setAttribute("ApplicationContext", ac);
     }
	
}
