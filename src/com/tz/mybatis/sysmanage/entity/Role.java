package com.tz.mybatis.sysmanage.entity;


/**
 * 
 * 类描述：角色对象  
 * 类名称：com.tz.mybatis.sysmanage.entity.Role      
 * 创建人：keven  
 * 创建时间：2016年12月17日 下午10:03:48
 * @version   V1.0
 */
public class Role implements java.io.Serializable{

 	
	private static final long serialVersionUID = 695779558924105254L;
	
	private Long roleId;
	private String roleName;
	
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
}
