package com.tz.mybatis.sysmanage.entity;

import java.sql.Timestamp;

/**
 * 跟数据库表M_USER表对应的实体类
 * @author Administrator
 *
 */
public class DeptUser implements java.io.Serializable {
	
	private static final long serialVersionUID = 1l;
	
	private Long deptId;
	private String deptName;	
	private Long parentDeptId;
	
	private Long userId;
	private String userName;
	
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public Long getParentDeptId() {
		return parentDeptId;
	}
	public void setParentDeptId(Long parentDeptId) {
		this.parentDeptId = parentDeptId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
