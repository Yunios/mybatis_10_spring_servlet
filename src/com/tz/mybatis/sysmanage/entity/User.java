package com.tz.mybatis.sysmanage.entity;

import java.sql.Timestamp;
import java.util.List;

/**
 * 跟数据库表M_USER表对应的实体类
 * @author Administrator
 *
 */
public class User implements java.io.Serializable {
	
	private static final long serialVersionUID = 7195902105451305867L;
	
	private Long userId;
	private String userName;
	private String loginName;
	private String password;
	private Long deptId;
	private Timestamp birthday;
	private Timestamp tvUpdate;
	
	private Dept dept;
	
	private List<Role> roleList;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Timestamp getBirthday() {
		return birthday;
	}
	public void setBirthday(Timestamp birthday) {
		this.birthday = birthday;
	}
	public Timestamp getTvUpdate() {
		return tvUpdate;
	}
	public void setTvUpdate(Timestamp tvUpdate) {
		this.tvUpdate = tvUpdate;
	}
	
	
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	 
	public List<Role> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", loginName=" + loginName + ", password="
				+ password + ", deptId=" + deptId + ", birthday=" + birthday + ", tvUpdate=" + tvUpdate + "]";
	}

}
