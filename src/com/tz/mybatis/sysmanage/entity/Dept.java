package com.tz.mybatis.sysmanage.entity;

import java.util.List;

/**
 * 跟数据库表M_USER表对应的实体类
 * @author Administrator
 *
 */
public class Dept implements java.io.Serializable {
	
	private static final long serialVersionUID = 1l;
	
	private Long deptId;
	private String deptName;	
	private Long parentDeptId;
	
	private List<User> userList;
	
	private List<Dept> childDeptList;
	
	
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public Long getParentDeptId() {
		return parentDeptId;
	}
	public void setParentDeptId(Long parentDeptId) {
		this.parentDeptId = parentDeptId;
	}
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	public List<Dept> getChildDeptList() {
		return childDeptList;
	}
	public void setChildDeptList(List<Dept> childDeptList) {
		this.childDeptList = childDeptList;
	}
	 
}
