package com.tz.mybatis.sysmanage.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tz.mybatis.sysmanage.entity.User;
import com.tz.mybatis.sysmanage.service.IUserService;

public class MybatisSpringTest {
	 
	private  ApplicationContext ac = null;
	
	@Before
	public void init(){
		ac = new ClassPathXmlApplicationContext("applicationContext.xml");
	}
	 
	//测试spring继承后mybatis的第二种开发方式(mapper的动态代理)
	@Test
	public void testGetUserList(){
		IUserService userService = (IUserService) ac.getBean("userService");		 
		List<User> userList = userService.getUserList();
		System.out.println(userList.size());
	}
	
}
