package com.tz.mybatis.sysmanage.servlet;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;

import com.tz.mybatis.sysmanage.service.IUserService;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
 
	 
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String username = request.getParameter("username");
		 String password = request.getParameter("password");
		 
		 if(username!=null&&password!=null){
			 //	去数据库验证此用户名和密码是否匹配,如果匹配,进入系统主页,登陆成功,如果失败则转发到失败页面
			ServletContext servletContext = this.getServletContext();
			ApplicationContext ac =
						(ApplicationContext) servletContext.getAttribute("ApplicationContext");
			IUserService userService = (IUserService) ac.getBean("userService");
			if(userService.login(username, password)){
				response.sendRedirect("main.jsp");
			}else{
				 request.getRequestDispatcher("error.jsp").forward(request, response);
			}
		
		 }else{
			 request.getRequestDispatcher("error.jsp").forward(request, response);
		 }
		
	}

}
