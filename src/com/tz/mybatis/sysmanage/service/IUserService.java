package com.tz.mybatis.sysmanage.service;

import java.util.List;

import com.tz.mybatis.sysmanage.entity.User;

/**
 * 用户增删改查服务层接口
 * @author Administrator
 *
 */
public interface IUserService {

	/**
	 * 查询所有用户信息
	 * @return
	 */
	public List<User> getUserList();
	
	public boolean login(String username,String password);
}
