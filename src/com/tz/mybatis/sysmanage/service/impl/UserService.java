package com.tz.mybatis.sysmanage.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tz.mybatis.sysmanage.entity.User;
import com.tz.mybatis.sysmanage.mapper.UserMapper;
import com.tz.mybatis.sysmanage.service.IUserService;

@Service
public class UserService implements IUserService{

	@Autowired
	private UserMapper userMapper;
	
	
	@Override
	public List<User> getUserList() {
		 return userMapper.getUserList();
	}
	
	
	public boolean login(String username,String password){
		boolean flag = false;
		User user = userMapper.getUserByLoginNameAndPsd(username, password);
		if(user!=null)
			flag = true;
		return flag;
		
	}


}
