package com.tz.mybatis.sysmanage.dto;

import java.util.List;

import com.tz.mybatis.sysmanage.entity.User;

/**
 * 跟数据库表M_USER表对应的实体类
 * @author Administrator
 *
 */
public class UserDto implements java.io.Serializable {
	
	private static final long serialVersionUID = 1l;
	 
	private User user;
	
	
	private List<Long> ids;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Long> getIds() {
		return ids;
	}

	public void setIds(List<Long> ids) {
		this.ids = ids;
	}
	 
	
	

}
