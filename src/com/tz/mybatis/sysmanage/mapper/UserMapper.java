package com.tz.mybatis.sysmanage.mapper;

import java.util.List;
import java.util.Map;

import com.tz.mybatis.sysmanage.dto.UserDto;
import com.tz.mybatis.sysmanage.entity.User;
import com.tz.mybatis.sysmanage.entity.UserDept;

/**
 * 定义用户增删改查dao接口
 * @author Administrator
 *
 */
public interface UserMapper {
	
	/**
	 * 根据用户id获取用户对象信息
	 * @param userId
	 * @return
	 */
	public User getUserById(Long userId) ;
	
	/**
	 * 通过登录名和密码来验证用户是否存在
	 * @param loginName
	 * @param password
	 * @return
	 */
	public User getUserByLoginNameAndPsd(String loginName,String password);
		
	/**
	 * 通过用户实体包装类来进行查询
	 * @param userDto
	 * @return
	 */
	public List<User> getUserListByUserDto(UserDto userDto);
	
	
	/**
	 * 获取所有用户数目
	 * @return
	 */
	public Integer getUserCount();
	
	
	/**
	 * 返回map结构的数据
	 * @return
	 */
	public List<Map<Object,Object>> getUserListMap();
	
	
	
	/**
	 * 用resultMap返回结果集的方式
	 */
	public List<User> getUserListByResultMap();
	
	/**
	 * 查询所有用户对象
	 * @return
	 */
	public List<User> getUserList();
	
	/**
	 * 增加用户对象
	 * @param user
	 */
	public void addUser(User user);
	
	
	/**
	 * 删除用户对象
	 * @param user
	 */
	public void delUser(User user);
	
	
	/**
	 * 修改用户对象
	 * @param user
	 */
	public void updateUser(User user);
	
	
	/**
	 * 查询所有的用户信息,包含部门名称信息
	 */
	public List<UserDept> getUserDeptList(); 
	
	/**
	 * 查询所有的用户信息,包含部门名称信息 ,用resultMap返回多对一关联关系的sql查询对象(User)
	 */
	
	public List<User> getUserDeptListReseultMap();
	
	
	public User getUserRoleListResultMap(Long userId);
	/**
	 * 根据用户id查询用户信息,关联部门名称显示
	 */
	public User  getUserByIdLazyLoad(Long userId);	
	
	/**
	 * 查询某个部门下面所有用户
	 * @param deptId
	 * @return
	 */
	public List<User> getUserListByDeptId(Long deptId);
	
}
