package com.tz.mybatis.sysmanage.mapper;

import java.util.List;

import com.tz.mybatis.sysmanage.entity.Dept;
import com.tz.mybatis.sysmanage.entity.DeptUser;

/**
 * 定义部门增删改查dao接口
 * @author Administrator
 *
 */
public interface DeptMapper {
	
	
	/**
	 * 根据部门id查询部门信息
	 * @param deptId
	 * @return
	 */
	public Dept getDeptById(Long deptId);
	
	/**
	 * 查询某个部门下面的所有用户信息
	 */
	public List<DeptUser> getDeptUserList(Long deptId); 
	
	
	/**
	 * 查询某个部门下面的所有用户信息,以resultMap返回
	 * @param deptId
	 * @return
	 */
	public Dept getDeptUserListResultMap(Long deptId);
	
	
	/**
	 * 查询某个部门下面所有的子部门
	 * @param deptId
	 * @return
	 */
	public List<Dept> getChildDeptByParentDeptId(Long deptId);
	
	/**
	 * 查询某个部门下面所有的子部门(用resultMap方式返回对象)
	 * @param deptId
	 * @return
	 */
	public Dept getChildByPidResultMap(Long deptId);
	
	
	/**
	 * 根据部门id查询部门信息(关联用户信息,用lazyLoad方式)
	 * @param deptId
	 * @return
	 */
	public Dept getDeptByIdLazyLoad(Long deptId);
	
	
	public Dept getChildDeptByPidLazyLoad(Long parentId);
}
