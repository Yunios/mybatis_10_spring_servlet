package com.tz.mybatis.sysmanage.dao;

import java.util.List;

import com.tz.mybatis.sysmanage.entity.User;

/**
 * 定义用户增删改查dao接口
 * @author Administrator
 *
 */
public interface IUserDao {
	
	/**
	 * 根据用户id获取用户对象信息
	 * @param userId
	 * @return
	 */
	public User getUserById(Long userId);
	
	
	/**
	 * 查询所有用户对象
	 * @return
	 */
	public List<User> getUserList();
	
	/**
	 * 增加用户对象
	 * @param user
	 */
	public void addUser(User user);
	
	
	/**
	 * 删除用户对象
	 * @param user
	 */
	public void delUser(User user);
	
	
	/**
	 * 修改用户对象
	 * @param user
	 */
	public void updateUser(User user);
	
	
		
}
