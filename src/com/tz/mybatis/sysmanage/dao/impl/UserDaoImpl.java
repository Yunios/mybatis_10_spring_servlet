package com.tz.mybatis.sysmanage.dao.impl;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.tz.mybatis.sysmanage.dao.IUserDao;
import com.tz.mybatis.sysmanage.entity.User;


@Repository
public class UserDaoImpl implements IUserDao{

	@Autowired
	private SqlSessionFactory sqlSessionFactory;
	
	 
	@Override
	public User getUserById(Long userId) {
		SqlSession sqlSession = sqlSessionFactory.openSession();		
		User user = sqlSession.selectOne("user.getUserById", 1l);		
		sqlSession.close();
		return user;
	}

	@Override
	public List<User> getUserList() {
		SqlSession sqlSession = sqlSessionFactory.openSession();		
		List<User> userList= sqlSession.selectList("user.getUserList");		
		sqlSession.close();
		return userList;
	}

	@Override
	public void addUser(User user) {
		SqlSession sqlSession = sqlSessionFactory.openSession();		
		int rows = sqlSession.insert("user.addUser", user);	
		System.out.println("受影响的行:"+rows);
		sqlSession.commit();
		sqlSession.close();		
		
	}

	@Override
	public void delUser(User user) {
		SqlSession sqlSession = sqlSessionFactory.openSession();		
		int rows = sqlSession.delete("user.delUser", user.getUserId());	
		System.out.println("受影响的行:"+rows);
		sqlSession.commit();
		sqlSession.close();	
		
	}

	@Override
	public void updateUser(User user) {	
		SqlSession sqlSession = sqlSessionFactory.openSession();		
		int rows = sqlSession.update("user.updateUser", user);	
		System.out.println("受影响的行:"+rows);
		sqlSession.commit();
		sqlSession.close();	
	}

}
