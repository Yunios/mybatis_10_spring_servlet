开发一个简单的登陆功能 通过servlet
	
思考: spring如何应用在web项目中
	在java工程中 ,我们可以通过加载spring的配置文件,初始化容器
	ac = new ClassPathXmlApplicationContext("applicationContext.xml");
	IUserService userService = (IUserService) ac.getBean("userService");	
	
	在web工程中 IOC容器怎么创建,创建后要怎么获取容器管理的对象
		IOC容器必须在web容器启动的时候创建
			1:servlet提供了ServletContextListener.contextInitialized方法
				可以在web容器启动的时候加载我们需要的东西
			2:创建ioc容器以后,将其放在ServletContext	
				servletContext.setAttribute("ApplicationContext", ac);
	